import lib from '../index.js'

var assert = require('assert')

var getBoundsOf2Boxes = lib.getBoundsOf2Boxes
var getBoundsOfManyBoxes = lib.getBoundsOfManyBoxes

let makebox = () => {
    return Object.assign({}, {
        latmin: 1,
        latmax: 2,
        lngmin: 1,
        lngmax: 2
    })
}

describe('Bbox', function () {
  describe('', function () {
    it('Bounding box of identical boxes', function () {
        let box1 = makebox()
        let box2 = makebox()
        let bounds = getBoundsOf2Boxes(box1, box2)
        assert.equal(box1.latmin, bounds.latmin)
        assert.equal(box1.latmax, bounds.latmax)
        assert.equal(box1.lngmin, bounds.lngmin)
        assert.equal(box1.latmax, bounds.latmax)
    })
    it('Bounding box when box1 contains box2', function () {
      let box1 = makebox()
      let box2 = makebox()
      box1.latmin = 0
      box1.latmax = 2
      box1.lngmin = 0
      box1.lngmax = 2
      let bounds = getBoundsOf2Boxes(box1, box2)
      assert.equal(box1.latmin, bounds.latmin)
      assert.equal(box1.latmax, bounds.latmax)
      assert.equal(box1.lngmin, bounds.lngmin)
      assert.equal(box1.latmax, bounds.latmax)
    })
    it('Bounding box when box2 contains box1', function () {
      let box1 = makebox()
      let box2 = makebox()
      box2.latmin = 0
      box2.latmax = 2
      box2.lngmin = 0
      box2.lngmax = 2

      let bounds = getBoundsOf2Boxes(box1, box2)
      assert.equal(box2.latmin, bounds.latmin)
      assert.equal(box2.latmax, bounds.latmax)
      assert.equal(box2.lngmin, bounds.lngmin)
      assert.equal(box2.latmax, bounds.latmax)
    })

    it('Bounding box when box2 contains box1', function () {
      let box1 = makebox()
      let box2 = makebox()
      let box3 = makebox()
      let bounds = getBoundsOfManyBoxes([box1, box2, box3])
      assert.equal(box1.latmin, bounds.latmin)
      assert.equal(box1.latmax, bounds.latmax)
      assert.equal(box1.lngmin, bounds.lngmin)
      assert.equal(box1.latmax, bounds.latmax)
    })

    it('Bounding box when box3 contains box1 and box2', function () {
      let box1 = makebox()
      let box2 = makebox()
      let box3 = makebox()
      box3.latmin = 0
      box3.latmax = 2
      box3.lngmin = 0
      box3.lngmax = 2
      let bounds = getBoundsOfManyBoxes([box1, box2, box3])
      assert.equal(box3.latmin, bounds.latmin)
      assert.equal(box3.latmax, bounds.latmax)
      assert.equal(box3.lngmin, bounds.lngmin)
      assert.equal(box3.latmax, bounds.latmax)
      assert.notEqual(box1.latmin, bounds.latmin)
    })
  })
})
