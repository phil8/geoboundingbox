function getBoundsOf2Boxes (box1, box2) {
  let box = box1
  // console.log(`${box.latmin} ${box.latmax} ${box.lngmin} ${box.lngmax}`)
  box = box2
  // console.log(`${box.latmin} ${box.latmax} ${box.lngmin} ${box.lngmax}`)
  let boundingbox = Object.assign({}, box1)

  if (box2.latmax > boundingbox.latmax) {
    boundingbox.latmax = box2.latmax
  }
  if (box2.lngmax > boundingbox.lngmax) {
    boundingbox.lngmax = box2.lngmax
  }
  if (box2.latmin < boundingbox.latmin) {
    boundingbox.latmin = box2.latmin
  }
  if (box2.lngmin < boundingbox.lngmin) {
      boundingbox.lngmin = box2.lngmin
  }
  box = boundingbox
  // console.log(`${box.latmin} ${box.latmax} ${box.lngmin} ${box.lngmax}`)
  return boundingbox
}

function getBoundsOfManyBoxes (bboxes) {
  let reducer = (boundingbox, bbox) => {
    if (!boundingbox) {
      return bbox
    } else {
      return getBoundsOf2Boxes(boundingbox, bbox)
    }
  }
  return bboxes.reduce(reducer, null)
}

export default {
  getBoundsOf2Boxes,
  getBoundsOfManyBoxes
}
